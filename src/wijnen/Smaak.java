package wijnen;

/**
 * PEER opdracht
 * P2W2
 */
public enum Smaak {
    BRUT("Brut"),
    EXTRA_BRUT("Extra-brut"),
    BRUT_SANS_MILLESIME("Brut sans Millésime"),
    SEC("Sec"),
    DEMI_SEC("Demi-sec"),
    DOUX("Doux");

    private String smaakNaam;

    Smaak(String smaakNaam) {
        this.smaakNaam = smaakNaam;
    }

    @Override
    public String toString() {
        return String.format("%s",smaakNaam);
    }
}
