package wijnen;

import java.time.LocalDate;

/**
 * PEER opdracht
 * P2W2
 */
public class Likeur extends Wijn {
    private double alcoholGehalte; //in procent


    public Likeur(String naam, String streek, LocalDate oogstDatum, double basisPrijs, double alcoholGehalte) {
        super(naam, streek, oogstDatum, basisPrijs);
        this.alcoholGehalte = alcoholGehalte;
    }

    @Override
    public double berekenPrijs() {
        if (alcoholGehalte > 0.5) {
            return this.getBasisPrijs() * 1.25;
        } else {
            return this.getBasisPrijs();
        }
    }

    @Override
    public String toString() {
        return String.format("%-40s € %5.2f \n \t (%s) --> %.0f%% alc", this.getNaam(), this.berekenPrijs(), this.getKenmerken(), this.alcoholGehalte * 100);
    }
}
