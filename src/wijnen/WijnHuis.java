package wijnen;

/**
 * PEER opdracht
 * P2W2
 */
public class WijnHuis {
    private static final int MAX_AANTAL = 10;
    private Wijn[] wijnen = new Wijn[MAX_AANTAL];  //voorlopig gevuld met 10 null-objecten
    private String naam;
    private int aantal = 0;

    public WijnHuis(String naam) {
        this.naam = naam;
    }

    public void voegWijnToe(Wijn wijn) {
        if (aantal < MAX_AANTAL && !zoekWijn(wijn)) {
            wijnen[aantal] = wijn;
            aantal++;
        }
    }

    public boolean zoekWijn(Wijn wijn) {
        boolean wijnExists = false;
        for (Wijn wijn1 : wijnen) {
            if (wijn1 != null) {
                if (wijn1.getNaam().toLowerCase().equals(wijn.getNaam().toLowerCase())) {
                    wijnExists = true;
                    break;
                }
            }
        }
        return wijnExists;
    }

    public Wijn getOudsteWijn() {
        int index = 0;
        int teller = 0;
        int year = 2020;
        for (Wijn wijn : wijnen) {
            if (wijn != null) {
                if (wijn.getOogstDatum().getYear() < year) {
                    year = wijn.getOogstDatum().getYear();
                    index = teller;
                }
            }
            teller++;
        }
        return wijnen[index];
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append(String.format("Wijnhuis %s\n", naam.toUpperCase()));

        StringBuilder wijnenTekst = new StringBuilder();
        StringBuilder champagneTekst = new StringBuilder();
        StringBuilder likeurenTekst = new StringBuilder();
        wijnenTekst.append("Wijnen: \n");
        champagneTekst.append("Champagnes: \n");
        likeurenTekst.append("Likeuren: \n");
        for (Wijn wijn : wijnen) {
            if (wijn != null) {
                if (wijn instanceof Likeur) {
                    Likeur likeur = (Likeur) wijn;
                    likeurenTekst.append(String.format("\t%s \n", likeur.toString()));
                }
                else if (wijn instanceof Champagne) {
                    Champagne champagne = (Champagne) wijn;
                    champagneTekst.append(String.format("\t%s \n", champagne.toString()));
                } else {
                    wijnenTekst.append(String.format("\t%s \n", wijn.toString()));
                }
            }
        }
        result.append(wijnenTekst.toString());
        result.append(champagneTekst.toString());
        result.append(likeurenTekst.toString());
        return result.toString();
    }
}
