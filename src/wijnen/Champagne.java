package wijnen;

import java.time.LocalDate;

/**
 * PEER opdracht
 * P2W2
 */
public class Champagne extends Wijn {
    private Smaak smaak;


    public Champagne(String naam, String streek, LocalDate oogstDatum, double basisPrijs,Smaak smaak) {
        super(naam, streek, oogstDatum, basisPrijs);
        this.smaak = smaak;
    }
    @Override
    public double berekenPrijs(){
        if (smaak.toString().toLowerCase().contains("brut")){
            return this.getBasisPrijs()*1.1;
        }
        else{
            return this.getBasisPrijs();
        }
    }
    @Override
    public String toString(){
        return String.format("%-40s € %5.2f \n \t (%s) --> Type: %s",this.getNaam(),this.berekenPrijs(),this.getKenmerken(),this.smaak.toString());
    }
}
